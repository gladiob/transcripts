# Sibel Edmonds & Company on Operation Gladio B: Chronological A/V Media Transcript Collection 

## [IN PROGRESS]

* * *

### 
**PREFATORY MATERIAL:**

*   **January 18, 2013:** "Tom Secker on Operation Gladio"  
   -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))  
    (Essential Gladio B Series Background)

    *   [Audio Interview (50:16)](http://www.corbettreport.com/interview-582-tom-secker-on-operation-gladio/) ([_BFP_](http://www.boilingfrogspost.com/2013/01/25/prelude-to-a-coming-interview-operation-gladio/))
    *   [**Transcript**](https://www.jottit.com/re8qt/)  
.

* * *

### GLADIO B SERIES:

**Sibel Edmonds Interviewed by James Corbett** ([_The Corbett Report_](http://www.corbettreport.com))

([YouTube Playlist, Parts 1-5](https://www.youtube.com/playlist?list=PLN6xa7kD9dZ_qXHAmm4dx-Bqqy10mSVDZ))

1.  **January 30, 2013 [Part 1]:** "Sibel Edmonds on NATO, Terrorism, 9/11 and Drug Running"

    *   [Audio Interview (67:50)](http://www.corbettreport.com/interview-595-sibel-edmonds-on-nato-terrorism-911-and-drug-running/) ([_BFP_](http://www.boilingfrogspost.com/2013/01/30/sibel-edmonds-on-nato-terrorism-911-and-drug-running/))
    *   [Video (67:50)](http://www.corbettreport.com/sibel-edmonds-on-gladio-b-part-1-video/) (posted February 20, 2013) ([_BFP_](http://www.boilingfrogspost.com/2013/02/19/video-report-sibel-edmonds-on-operation-gladio-part-i/)) ([YouTube](https://www.youtube.com/watch?v=AARtO88G5Ag))
    *   [**Subtitled/Captioned Video**](http://www.amara.org/en/videos/Lpuxr3NoBEJa/info/sibel-edmonds-on-gladio-b-part-1/) ([DFXP [EN]](http://pastebin.com/F9AF6WSa)) ([WebVTT [EN]](http://pastebin.com/6bxTr1Dq)) ([TXT [EN]](http://pastebin.com/rGYrWKbc)) ([FR] forthcoming)
    *   [**Transcript**](https://www.jottit.com/29u85/)
    *   **[Transcript as published by _The Corbett Report_, October 22, 2014](http://www.corbettreport.com/an-introduction-to-gladio-b/)**  
.


2.  **February 8, 2013 [Part 2]:** "Sibel Edmonds on Gladio B, Protected Terrorists and Stifled Investigations"

    *   [Audio Interview (58:20)](http://www.corbettreport.com/interview-598-sibel-edmonds-on-gladio-b-protected-terrorists-and-stifled-investigations/),  ([_BFP_](http://www.boilingfrogspost.com/2013/02/08/corbett-report-sibel-edmonds-on-gladio-b-protected-terrorists-stifled-investigations/))
    *   [Video (58:20)](http://www.corbettreport.com/sibel-edmonds-on-gladio-b-part-2-video/) (posted February 20, 2013) ([_BFP_](http://www.boilingfrogspost.com/2013/02/20/video-report-sibel-edmonds-on-operation-gladio-part-ii/)) ([YouTube](https://www.youtube.com/watch?v=RHbqhy2DSxE))
    *   [**Subtitled/Captioned Video**](http://amara.org/en/videos/a0HaeySMyrcn/info/sibel-edmonds-on-gladio-b-part-2/) ([DFXP [EN]](http://pastebin.com/JjAz5pwF)) ([WebVTT [EN]](http://pastebin.com/xAbKtYRz)) ([TXT [EN]](http://pastebin.com/a6Ri3N7a))
    *   [**Transcript**](https://www.jottit.com/af2hf/)
    *   **[Transcript as published by _The Corbett Report_, October 29, 2014](http://www.corbettreport.com/gladio-b-protecting-terrorists-and-stifling-investigations/)**  
.


3.  **February 15, 2013 [Part 3]:** "Sibel Edmonds on Turkey, the Hood Event, Israel and Gladio B"

    *   [Audio Interview (66:12)](http://www.corbettreport.com/interview-604-sibel-edmonds-on-turkey-the-hood-event-israel-and-gladio-b/) ([_BFP_](http://www.boilingfrogspost.com/2013/02/15/corbett-report-sibel-edmonds-on-gladio-part-iii-turkey-iraq-the-hood-event-israel-gladio-b/))
    *   [Video (66:12)](http://www.corbettreport.com/sibel-edmonds-on-gladio-b-part-3-video/) (posted February 21, 2013) ([_BFP_](http://www.boilingfrogspost.com/2013/02/21/video-report-sibel-edmonds-on-operation-gladio-part-iii/)) ([YouTube](https://www.youtube.com/watch?v=D4zIQKAj7gM))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/cKOv6Df90LUa/info/sibel-edmonds-on-gladio-b-part-3/))
    *   [**Transcript**](https://www.jottit.com/6qktv/)
    *   **[Transcript as published by _The Corbett Report_, November 2, 2014](http://www.corbettreport.com/gladio-b-from-nationalist-terror-to-islamic-terror/)**  
.

4.  **February 22, 2013 [Part 4]:** "Sibel Edmonds on Gladio Protected Drug Running and Money Laundering"

    *   [Audio Interview (74:52)](http://www.corbettreport.com/interview-610-sibel-edmonds-on-gladio-protected-drug-running-and-money-laundering/)
    *   [Video (74:52)](http://www.corbettreport.com/sibel-edmonds-on-gladio-b-part-4-video/) (posted February 23, 2013) ([_BFP_](http://www.boilingfrogspost.com/2013/02/23/video-report-sibel-edmonds-on-operation-gladio-part-iv/)) ([YouTube](https://www.youtube.com/watch?v=mOCYMU3zYH0))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/pGAPMTxukfv2/info/sibel-edmonds-on-gladio-b-part-4/))
    *   [**Transcript**](https://www.jottit.com/7qedm/)
    *   **[Transcript as published by _The Corbett Report_, November 7, 2014](http://www.corbettreport.com/gladio-b-running-drugs-and-laundering-money/)**  
.

5.  **March 1, 2013 [Part 5]:** "Sibel Edmonds Answers Your Questions on Gladio B"

    *   [Audio Interview (92:48)](http://www.corbettreport.com/interview-616-sibel-edmonds-answers-questions-on-gladio-b/)
    *   [Video (92:48)](http://www.corbettreport.com/sibel-edmonds-answers-your-questions-on-gladio-b/) (posted March 1,2013) ([_BFP_](http://www.boilingfrogspost.com/2013/03/01/video-report-operation-gladio-q-a/)) ([YouTube](https://www.youtube.com/watch?v=zccmyXIRfxU))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/qSOxcriFphni/info/sibel-edmonds-answers-your-questions-on-gladio-b/))
    *   [**Transcript**](https://www.jottit.com/umk5g/)
    *   **[Transcript as published by _The Corbett Report, November 11, 2014_](http://www.corbettreport.com/sibel-edmonds-answers-your-questions-on-gladio-b-2/)  
.

    * * *

6.  **March 15, 2013 [(Unofficial) Part 6]:** "Sibel Edmonds Explains Who’s At The Top of the Pyramid"

    *   [Audio Interview (68:43)](http://www.corbettreport.com/interview-626-sibel-edmonds-explains-whos-at-the-top-of-the-pyramid/)
    *   [Video (68:43)](http://www.corbettreport.com/sibel-edmonds-explains-whos-at-the-top-of-the-pyramid-video/), March 16, 2013 ([_BFP_](http://www.boilingfrogspost.com/2013/03/15/corbett-video-report-gladio-series-whos-at-the-top-of-the-pyramid/)) ([YouTube](https://www.youtube.com/watch?v=rgo-E7KhVAc))
    *   **Subtitled/Captioned Video** ([in process](http://www.amara.org/en/videos/Bdsxj7a4C3gU/info/sibel-edmonds-explains-whos-at-the-top-of-the-pyramid/))
    *   [**Transcript**](https://www.jottit.com/juxv3/)  
.

* * *

### FOLLOW-UP MATERIAL:

(items of particular importance to be denoted by "*" as they are identified)

1.  **February 1, 2013:** "Gladio B Revisited"  
    -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))  
Excerpts Tom Secker [01/18/2013] and Part 1 [01/30/2013] interviews, above

    *   [Audio Report (57:13)](http://www.corbettreport.com/episode-256-gladio-revisited/)
    *   [Video (57:13)](http://www.corbettreport.com/gladio-revisited-video/), February 3 ([_BFP_](http://www.boilingfrogspost.com/2013/02/02/must-watch-video-operation-gladio-part-i-from-neo-fascists-to-mujahedeen-aka-al-qaeda/)) ([YouTube](https://www.youtube.com/watch?v=bsWHafahbvs)):
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/Qo3Jx5A9NtTH/info/gladio-revisited/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/wzf4iWCe))
    *   [**Transcript**](https://www.jottit.com/jq8vj/)  
.

2.  **February 16, 2013:** "Know Your Terrorists: Ayman Al-Zawahiri"  
    -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Audio Report (50:35)](http://www.corbettreport.com/episode-258-know-your-terrorists-ayman-al-zawahiri/)
    *   [Video (50:35)](http://www.corbettreport.com/know-your-terrorists-ayman-al-zawahiri-video/) February 16 ([YouTube](https://www.youtube.com/watch?v=f-rYMjgs2nE))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/0rHawCHaEYe3/info/know-your-terrorists-ayman-al-zawahiri/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/yHgeKqN9))
    *   _Transcript (proposed)_  
.

3.  **March 14, 2013:** "Terror in Central Asia: NATO’s Great Game"  
    -- by James Corbett ([_GRTV_ | GlobalResearch.ca](http://tv.globalresearch.ca/))

    *   [Video Report (14:09)](http://www.corbettreport.com/terror-in-central-asia-natos-great-game/) ([_GRTV_](http://tv.globalresearch.ca/2013/10/terror-central-asia-nato%E2%80%99s-great-game)) ([YouTube](https://www.youtube.com/watch?v=zvkHccJny54))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/i86KNjKGx5ke/info/terror-in-central-asia-natos-great-game/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/JUT6s68Q))
    *   _Transcript (proposed)_  
.

4.  ***April 23, 2013:** "Sibel Edmonds on the Boston Bombing: The US Roots of 'Chechen' Terrorism"  
    -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Audio Interview (45:24)](http://www.corbettreport.com/interview-655-sibel-edmonds-on-the-boston-bombing/)
    *   [Video (45:24)](http://www.corbettreport.com/sibel-edmonds-on-the-boston-bombing-the-us-roots-of-chechen-terrorism/), April 24 ([_BFP_](http://www.boilingfrogspost.com/2013/04/23/the-eyeopener-sibel-edmonds-on-the-boston-bombing-the-us-roots-of-chechen-terrorism/)) ([YouTube](https://www.youtube.com/watch?v=2RCN1w5J80E))
    *   **Subtitled/Captioned Video** ([in process](http://www.amara.org/en/videos/HsXSTw8TJanj/info/sibel-edmonds-on-the-boston-bombing-the-us-roots-of-chechen-terrorism/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/v989TVfQ))
    *   [**Transcript**](https://www.jottit.com/k4t62/)  
.

5.  **April 29, 2013: ** "Terror by Design: 'Gladio B' and The Boston Bombings"  
    -- by David Knight (_[The Infowars Nightly News](http://tv.infowars.com/index/channel/category/nnews)_ | [Infowars.com](http://www.infowars.com))  

    *   [Video Interview (40:48)](https://www.youtube.com/watch?v=pDRZiGVLaJc)([YouTube [full newscast; segment @59m:09s]](https://www.youtube.com/watch?v=fM2cE_e9G4c&feature=youtu.be#t=59m09s))
    *   **Subtitled/Captioned Video** ([_proposed_]())
    *   _Transcript (proposed)_  
.

6.  **April 30, 2013:** "The Boston Bombing, the CIA, and the US Empire | Sibel Edmonds"  
    -- by Lew Rockwell ([LewRockwell.com](http://www.lewrockwell.com))

    *   [Audio Podcast (48:08)](http://www.lewrockwell.com/podcast/365-the-boston-bombing-the-cia-and-the-us-empire/)
([YouTube](https://www.youtube.com/watch?v=ZpmRWYLeOTc))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/cZdHfe47p8yR/info/365-the-boston-bombing-the-cia-and-the-us-empire-sibel-edmonds/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/qxVyC4vb))
    *   [Transcript (third-party)](http://www.lewrockwell.com/2013/08/no_author/the-boston-bombing-the-cia-and-the-us-empire/)  
.

7.  **May 7, 2013:** "Podcast Show #108: Dissecting Boston Terror Attack, Syria, Russia and Much More"  
    -- by [_Boiling Frogs Post_](http://www.boilingfrogspost.com)
    *   [Audio Podcast (64:29)](http://www.boilingfrogspost.com/2013/05/07/podcast-show-108-dissecting-boston-terror-attack-syria-russia-and-much-more/)
    *   _Transcript (proposed)_  
.

8.  **May 8, 2013:** "Who is Graham Fuller?"  
    -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Video Report (11:20)](http://www.corbettreport.com/who-is-graham-fuller/)  ([BFP](http://www.boilingfrogspost.com/2013/05/08/the-eyeopener-who-is-graham-fuller/)) ([YouTube](https://www.youtube.com/watch?v=ZlFhBrMaMsc))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/7kKuNYylkXjY/info/who-is-graham-fuller/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/BNsJNeNW))
    *   _Transcript (proposed)_  
.

9.  **May 23, 2013:** "Crisis of Civilization Podcast episode 3 (part 1): Sibel Edmonds"  
    -- by Dean Puckett and Nafeez Mosaddeq Ahmed ([CrisisOfCivilization.com](http://crisisofcivilization.com))

    *   [Audio Podcast (47:20)](http://crisisofcivilization.com/crisis-of-civilization-podcast-episode-3-part-1-sibel-edmonds/) ([BFP](http://www.boilingfrogspost.com/2013/05/21/podcast-interview-sibel-edmonds-on-the-u-s-governments-support-for-international-terrorism-heroin-organized-crime/))
    *   _Transcript (proposed):_  
.

10.  **May 23, 2013:** "Podcast Show #109: From the Sunday Times’ Spiked Report to the Boston Terror Attack and Syria"  
    -- by [_Boiling Frogs Post_](http://www.boilingfrogspost.com)

    *   [Audio Podcast (66:31)](http://www.boilingfrogspost.com/2013/05/23/podcast-show-109-from-the-sunday-times-spiked-report-to-the-boston-terror-attack-and-syria/)
    *   _Transcript (proposed)_  
.

11.  **June 10, 2013:** "Podcast Show #110: From Police State USA to American Apathy, Turkey’s Uprising & Beyond"  
   -- by [_Boiling Frogs Post_](http://www.boilingfrogspost.com)

    *   [Audio Podcast (75:21)](http://www.boilingfrogspost.com/2013/06/10/podcast-show-110-from-police-state-usa-to-american-apathy-turkeys-uprising-beyond/)
    *   _Transcript (proposed)_  
.

12.  **August 29, 2013:** "Pepe Escobar on Bandar Bush’s Role in Syria"  
    -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Audio Interview (26:05)](http://www.corbettreport.com/interview-733-pepe-escobar-on-bandar-bushs-role-in-syria/)*   
    *_Transcript (proposed)_  
.

13.  **September 6, 2013:** "Sibel Edmonds on the Three Musketeers of State Dept Terrorism"  
    -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Audio Interview (41:43)](http://www.corbettreport.com/interview-743-sibel-edmonds-on-the-three-musketeers-of-state-dept-terrorism/)*   
    *[**Transcript (draft)**](https://www.jottit.com/69ncx/)  
.

14.  **September 6, 2013:** "Podcast Show #115: Obama, Putin, Prince Bandar & the Looming War on Syria"  
   -- by [_Boiling Frogs Post_](http://www.boilingfrogspost.com)

    *   [Audio Podcast (58:27)](http://www.boilingfrogspost.com/2013/09/06/podcast-show-115-obama-putin-prince-bandar-the-looming-war-on-syria/)*   
    *_Transcript (proposed)_  
..

15.  **November 6, 2013:** "An Introduction to Gladio B - James Corbett on Breaking The Set"  
   -- by Abby Martin ([_Breaking The Set_ | RT)](http://rt.com/shows/breaking-set-summary/)

    *   [Video Interview (9:18)](http://www.corbettreport.com/an-introduction-to-gladio-b-james-corbett-on-breaking-the-set/) ([YouTube](https://www.youtube.com/watch?v=U0CQloZQoIc)) ([_RT_](http://rt.com/shows/breaking-set-summary/voting-marijuana-legalization-gmo-285/))
    *   **[Subtitled/Captioned Video](http://www.amara.org/en/videos/tguuyoDIEi3M/info/an-introduction-to-gladio-b-james-corbett-on-breaking-the-set/) **([DFXP [EN]](http://pastebin.com/tX5FQdWK)) ([WebVTT [EN]](http://pastebin.com/FtqxR2iH)) ([TXT [EN]](http://pastebin.com/Eu4Qp4eG))
    *   [**Transcript (draft) **](https://www.jottit.com/j2fcg/)  
.

16.  **November 26, 2013:** "Podcast Show #119- Talking Turkey & The Possible Machiavellian Angle in the Latest Developments on Iran"  
    -- by [_Boiling Frogs Post_](http://www.boilingfrogspost.com)

    *   [Audio Podcast (64:10)](http://www.boilingfrogspost.com/2013/11/26/podcast-show-119-talking-turkey-the-possible-machiavellian-angle-in-the-latest-developments-on-iran/)
    *   _Transcript (proposed)_  
.

17.  **January 21, 2014:** "Sibel Edmonds Explains Erdogan's Fall From Grace"  
   -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Video Interview (56:11) ](http://www.corbettreport.com/interview-809-sibel-edmonds-explains-erdogans-fall-from-grace/) ([_BFP_](http://www.boilingfrogspost.com/2014/01/21/bfp-eyeopener-report-sibel-edmonds-on-the-cias-reverse-engineering-of-turkeys-erdogan/)) ([YouTube](https://www.youtube.com/watch?v=WAiAFqXPwZ8))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/YhQORS6ooocW/info/sibel-edmonds-explains-the-cias-reverse-engineering-of-erdogan/))
([Raw TXT dump of uncorrected draft](http://pastebin.com/WgENiRfF))
    *   [**Transcript**](https://www.jottit.com/m6sbp/)  
.

18.  **April 2, 2014:** "Sibel Edmonds Breaks Down the Turkish False Flag Leak"  
    -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Audio Interview (29:47)](http://www.corbettreport.com/interview-852-sibel-edmonds-breaks-down-the-turkish-false-flag-leak/)*   
    *[**Transcript (uncorrected draft)**](https://www.jottit.com/rbspv/)  
.

19.  **April 2, 2014:** "The EyeOpener Report- Sibel Edmonds Examines the Turkish False Flag Leak"  
    -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Video Report (12:33)](http://www.boilingfrogspost.com/2014/04/02/the-eyeopener-report-sibel-edmonds-examines-the-turkish-false-flag-leak/) ([YouTube](https://www.youtube.com/watch?v=StJ6Yl7xW1k))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/bBm5RifOzS7u/info/examining-the-turkish-false-flag-leak-the-eyeopener/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/SHFMCVDB))
    *   **Transcript (pending)**  
.

20.  **April 11, 2014:** "BFP Roundtable Video 5– Our Takes on NATO, Russia, Turkey & the 'New Cold War'"  
   -- by [_Boiling Frogs Post_](http://www.boilingfrogspost.com)

    *   [Video Roundtable Discussion (54:55)](http://www.boilingfrogspost.com/2014/04/11/bfp-roundtable-video-5-our-takes-on-nato-russia-turkey-the-new-cold-war/) ([YouTube](https://www.youtube.com/watch?v=5gaosVLUclw))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/JB0Dmv9iKQRO/info/the-bfp-roundtable-takes-on-nato-russia-turkey-and-the-new-cold-war/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/Nr5QnBb3))
    *   [**Transcript (draft)**](https://www.jottit.com/agjr3/)  
.

21.  **May 6, 2014: **"Christoph Germann Explains China’s War on Terror"  
  -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Audio Interview (26:21)](http://www.corbettreport.com/interview-878-christoph-germann-explains-chinas-war-on-terror/)*   
    *   _Transcript (proposed)_  
.

22.  **May 7, 2014: **"Pepe Escobar Breaks Down China’s Uyghur Problem"  
  -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Audio Interview (19:37)](http://www.corbettreport.com/interview-879-pepe-escobar-breaks-down-chinas-uyghur-problem/)*   
    *   _Transcript (proposed)_  
.

23.  **May 8, 2014:** "Xinjiang, Azerbaijan and Gladio B with Christoph Germann"  
   -- by Pearse Redmond ([_Porkins Policy Radio | Porkins Policy Review_](https://porkinspolicyreview.wordpress.com/category/porkins-policy-radio/))

    *   [Audio Interview (52:21)](http://porkinspolicyreview.wordpress.com/2014/05/08/porkins-policy-radio-ep-22-xinjiang-azerbaijan-and-gladio-b-with-christoph-germann/) ([YouTube](https://www.youtube.com/watch?v=aoVj5qjv0y0))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/uqZYwzIPwVNL/info/porkins-policy-radio-episode-22-xinjiang-azerbaijan-and-gladio-b-with-christoph-germann-youtube/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/NzWesZxy))
    *   _Transcript (proposed)_  
.

24.  **May 13, 2014:** "China's 'War on Terror'" a.k.a. "The EyeOpener Report: The Real Roots of Xinjiang Terror"  
   -- by James Corbett ([_The EyeOpener Report_ | _Boiling Frogs Post_](http://www.boilingfrogspost.com/category/the_eyeopener_report/))
   
    *   [Video Report (11:46)](http://www.corbettreport.com/chinas-war-on-terror/) ([_BFP_](http://www.boilingfrogspost.com/2014/05/13/the-eyeopener-report-the-real-roots-of-xinjiang-terror/)) ([YouTube](https://www.youtube.com/watch?v=Rhb87YbsX7E))
    *   [**Subtitled/Captioned Video**](http://amara.org/en/videos/roWG4v96RqdR/info/the-real-roots-of-xinjiang-terror/) ([DFXP [EN]](http://pastebin.com/grAz9kFb)) ([WebVTT [EN]](http://pastebin.com/HbHQ9SNf)) ([TXT [EN]](http://pastebin.com/8V1zzEzW))
    *   [**Transcript**](https://www.jottit.com/rdbgr/)  
.

25.  **August 6, 2014:** "The EyeOpener Report- PM Erdogan’s Mega Enemies & the Turkish Election"  
    -- by James Corbett ([_The EyeOpener Report_ | _Boiling Frogs Post_](http://www.boilingfrogspost.com/category/the_eyeopener_report/))

    *   [Video Interview (58:24)](http://www.boilingfrogspost.com/2014/08/06/the-eyeopener-report-pm-erdogans-mega-enemies-the-turkish-election/) ([YouTube](https://www.youtube.com/watch?v=BpoVPH2qgEg))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/LCqBPSxTz4RH/info/erdogans-enemies-and-the-turkish-election-the-eyeopener/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/T6Lic0Px))
    *   _Transcript (proposed)_  
.

26.  **September 26, 2014:** "ISIS is everywhere in Central Asia"  
   --  by Pearse Redmond and Christoph Germann ([_Porkins Great Game | Porkins Policy Review_](https://porkinspolicyreview.wordpress.com/category/porkins-great-game/))
    *   [Audio Podcast (71:23)](https://porkinspolicyreview.wordpress.com/2014/09/26/porkins-great-game-ep-1-isis-is-everywhere-in-central-asisa/) ([YouTube](https://www.youtube.com/watch?v=_mkQtvOOohg))
    *   **Subtitles/Captioned Video** ([in process](http://amara.org/en/videos/snr5ZrgW9mB8/info/porkins-great-game-ep-1-isis-is-everywhere-in-central-asia/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/ffLJKYfQ))
    *   _Transcript (proposed)_  
.

27.  **September 27, 2014:** "Who is Really Behind ISIS?"  
    -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Audio Report (64:32)](http://www.corbettreport.com/episode-295-who-is-really-behind-isis/)
    *   [Video (64:32)](http://www.corbettreport.com/who-is-really-behind-isis-video/) ([YouTube](https://www.youtube.com/watch?v=8LSIwvE0Nvo))

    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/NZ9XHv95gCXz/info/who-is-really-behind-isis/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/gF3NXfX8))
    *   _Transcript (proposed)_  
.

28.  **October 22, 2014:** "The Beard World Order Ep. 5: The ISIS Conspiracy"  
    -- by Guillermo Jimenez, James Corbett, and Peter B. Collins (_[The Beard World Order | Traces of Reality](http://tracesofreality.com/tag/bwo/)_)
    *   [Video Roundtable (56:31)](http://tracesofreality.com/2014/10/22/beard-world-order-episode-05-the-isis-conspiracy/) ([_TCR_](http://www.corbettreport.com/interview-960-the-beard-world-order-on-the-isis-conspiracy/)) ([YouTube](https://www.youtube.com/watch?v=Y9RIwLHt7ro))
    *   **Subtitles/Captioned Video** ([in process](http://amara.org/en/videos/gfR4Stpw7seU/info/beard-world-order-episode-05-the-isis-conspiracy/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/FU8YYiyc))
    *   _Transcript (proposed)_  
.

29.  **October 31, 2014:** "Crisis in the Caucasus and Mysterious Crashes"  
    -- by Pearse Redmond and Christoph Germann ([_Porkins Great Game | Porkins Policy Review_](https://porkinspolicyreview.wordpress.com/category/porkins-great-game/))
    *   [Audio Podcast (64:58)](https://porkinspolicyreview.wordpress.com/2014/10/31/porkins-great-game-ep-2-crisis-in-the-caucasus-and-mysterious-crashes/) ([YouTube](https://www.youtube.com/watch?v=mFqNOy8lhOk))
    *   **Subtitles/Captioned Video** ([in process](http://www.amara.org/en/videos/BDVWLN4Bfizf/info/porkins-great-game-ep-2-crisis-in-the-caucasus-and-mysterious-crashes/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/fjYjMYmA))
    *   _Transcript (proposed)_  
.

30.  **November 1, 2014:** "BFP Roundtable Takes on “Islamic Terror:" Selling the 'ISIS' Brand"  
    -- by [_Boiling Frogs Post_](http://www.boilingfrogspost.com/)

    *   [Video Roundtable (69:47)](http://www.boilingfrogspost.com/2014/11/01/bfp-roundtable-takes-on-islamic-terror/) ([_Corbett Report_)](http://www.corbettreport.com/new-cold-war-same-old-tricks-bfp-roundtable-05/) ([YouTube](https://www.youtube.com/watch?v=fV1LkPcmDz4))
    *   **Subtitled/Captioned Video** ([in process](http://www.amara.org/en/videos/EahQiwQp7oFb/info/the-bfp-roundtable-takes-on-the-islamic-terror-hype/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/TSR3fMFh))
    *   [**Transcript (uncorrected draft)**](https://www.jottit.com/z2wxk/)  
.

31.  **November 23, 2014:** "James Corbett on The Secret War – Gladio and the Battle for Eurasia"  
     --  by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Videorecording of Lecture (64:54)](http://www.lpamsterdam.nl/2014/11/20/james-corbett-secret-war-gladio-battle-eurasia/) (courtesy [We Are Change Rotterdam](https://www.facebook.com/WeAreChangeRotterdam)), speaking at invitation of [Libertarische Partij Amsterdam](http://www.lpamsterdam.nl/) Cafe De Heffer, Amsterdam  ([YouTube](https://www.youtube.com/watch?v=vRd199HoIKw))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/2ezQWzo4HU19/info/james-corbett-in-amsterdam-the-secret-war-gladio-and-the-battle-for-eurasia/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/JzB6xNAp))
    *   _Transcript (proposed)_  
.

32.   **November 27, 2014 (posted):** "James Corbett on Gladio B, Open Source Investigation, Motivation"  
   -- by [Stanley Hakkietakkie](https://www.youtube.com/channel/UCRQ8tmGXQvgqxZ721EWBb1Q)

    *   [Video Interview (10:44)](http://www.corbettreport.com/james-corbett-on-gladio-b-open-source-investigation-motivation/)([YouTube 1](https://www.youtube.com/watch?v=b7J8E2q3tXw)) ([YouTube 2](https://www.youtube.com/watch?v=hGi9-BGNDcE))
    *   **[Subtitled/Captioned Video](http://amara.org/en/videos/UFXhvDfd8Uo2/en/860814/)** ([DFXP](http://pastebin.com/qgQYWBiV)) ([WebVTT](http://pastebin.com/J5WF8uM2)) ([SRT](http://pastebin.com/xyX6VuAi)) ([TXT](http://pastebin.com/kPBPVeqw))
    *   _Transcript (proposed):_  
.

33.  **November 19, 2014 (delivered); December 2, 2014 (posted):** "Gladio B and the Battle for Eurasia"  
   -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Videorecording of Lecture (53:32)](http://www.corbettreport.com/episode-298-gladio-b-and-the-battle-for-eurasia/)  delivered at [Studium Generale Groningen, University of Groningen](http://www.sggroningen.nl/en/evenement/secret-war-gladio-and-battle-eurasia)([YouTube](https://www.youtube.com/watch?v=3q0qau8f0w8))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/IXpYnECaPXMg/info/gladio-b-and-the-battle-for-eurasia/))
    *   _Transcript (proposed)_  
.

34.  **November 19, 2014 (delivered); December 4, 2014 (posted):** "Gladio B and the Battle for Eurasia: Q and A"  
   -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Audio Question and Answer Session (36:58)](http://www.corbettreport.com/interview-971-gladio-b-q-and-a/)*  
    *   _Transcript (proposed)_  
.

35.  **December 4, 2014 (posted):** "Financial Survival: Central Asia, Graveyard of Empires?"  
   -- by Frank Adask ([_Financial Survival_](https://adask.wordpress.com/radio/))

    *   [Audio Interview (32:24)](http://www.corbettreport.com/interview-971-gladio-b-q-and-a/)  with James Corbett ([_The Corbett Report_](http://www.corbettreport.com))
    *   _Transcript (proposed)_  
.

* * *

### FICTIONALIZATION: [_The Lone Gladio_](http://www.thelonegladio.com/) by Sibel Edmonds:

1.  **September 10, 2014:** "The Government Gagged Her, But It Didn’t Work: Sibel Edmonds"  
  -- by Lew Rockwell ([LewRockwell.com](http://www.lewrockwell.com))

    *   [Audio Interview (29:10)](http://www.lewrockwell.com/podcast/the-government-gagged-her-but-it-didnt-work/)([_BFP_](http://www.boilingfrogspost.com/2014/09/12/the-lone-gladio-lew-rockwell-interviews-sibel-edmonds/)) ([YouTube](https://www.youtube.com/watch?v=-Z24Mq-_Y-8))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/VUm2cnXC22MN/info/412-the-government-gagged-her-but-it-didnt-work-sibel-edmonds/))
    *   _Transcript (proposed)_  
..

2.  **September 13, 2014:** "Sibel Edmonds Speaks Truth Through Fiction in _The Lone Gladio"_  
   -- by Guillermo Jimenez ([_Traces of Reality_](http://tracesofreality.com/))

    *   [Audio Interview (91:55)](http://tracesofreality.com/2014/09/13/sibel-edmonds-speaks-truth-through-fiction-in-the-lone-gladio/) ([_BFP_](http://www.boilingfrogspost.com/2014/09/14/traces-of-reality-sibel-edmonds-speaks-truth-through-fiction-in-the-lone-gladio/))
    *   [**Transcript (uncorrected draft)**](https://www.jottit.com/tpvtk/)  
.

3.  **September 25, 2014:** "_The Lone Gladio_ Reviewed"  
 -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [ Video Review (23:16)](http://www.corbettreport.com/the-lone-gladio-reviewed/) ([_BFP_](http://www.boilingfrogspost.com/2014/09/24/corbett-report-video-the-lone-gladio-reviewed/)) ([YouTube](https://www.youtube.com/watch?v=SkPJ1Kh-o_A))
    *   **Subtitled/Captioned Video** ([in process](http://www.amara.org/en/videos/dYWTb6HEFDSi/info/the-lone-gladio-reviewed/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/yayzEzU9))
    *   [**Transcript**](https://www.jottit.com/njv5k/)  
.

4.  **October 15, 2014:** "Sibel Edmonds Explains _The Lone Gladio_"  
   -- by James Corbett ([_The Corbett Report_](http://www.corbettreport.com))

    *   [Audio Interview (60:35)](http://www.corbettreport.com/interview-954-sibel-edmonds-explains-the-lone-gladio/)*   [Video (60:35),](http://www.corbettreport.com/sibel-edmonds-on-the-lone-gladio-video/) October 16 ([_BFP_](http://www.boilingfrogspost.com/2014/10/15/the-eyeopener-report-sibel-edmonds-on-the-lone-gladio/)) ([YouTube](https://www.youtube.com/watch?v=0tK7yI23AWE))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/98nF0u4AOwFP/info/sibel-edmonds-explains-the-lone-gladio/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/gUKGgCVT))
    *   [**Transcript (draft)**](https://www.jottit.com/jcvss/)  
.

5.  **October 17, 2014:** "False Flags and the US War Against Islam with Sibel Edmonds," a.k.a. "_The Lone Gladio_, Synthetic Terrorism & Perpetual Wars," a.k.a. "FBI Whistleblower Says CIA Commissioned New Islamic Militant Cells to Orchestrate Terror Attacks"  
  -- by Sean Stone ([_Buzzsaw_ | TheLip.tv](http://thelip.tv/show/buzzsaw/))

    *   [Video Interview (51:28)](http://thelip.tv/episode/fbi-whistleblower-sibel-edmonds-cia-commissioned-new-islamic-militant-cells-orchestrate-terror-attacks/)  ([_BFP_](http://www.boilingfrogspost.com/2014/10/17/lip-tv-buzzsaws-sean-stone-operation-gladio-false-flags-the-us-war-against-islam-with-sibel-edmonds/)) ([YouTube](https://www.youtube.com/watch?v=uz677koGfPU))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/eaBABha9t6G2/info/false-flags-and-the-us-war-against-islam-with-sibel-edmonds/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/ubaeJNUu))
    *   _Transcript (proposed)_  
.

6.  **October 18, 2014:** "_The Lone Gladio_ with Sibel Edmonds"  
   -- by Pearse Redmond ([_Porkins Policy Radio | Porkins Policy Review_](https://porkinspolicyreview.wordpress.com/category/porkins-policy-radio/))

    *   [Audio Interview (48:56)](https://porkinspolicyreview.wordpress.com/2014/10/18/porkins-policy-radio-ep-29-the-lone-gladio-with-sibel-edmonds/) ([_BFP_](http://www.boilingfrogspost.com/2014/10/23/the-lone-gladio-a-political-spy-thriller-reviews-interviews/)) ([YouTube](https://www.youtube.com/watch?v=qNNi3nX_sN4))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/uIKciMzyN4U3/info/porkins-policy-radio-episode-29-the-lone-gladio-with-sibel-edmonds/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/hDzYWxxQ))
    *   _Transcript (proposed)_  
.

7.  **October 19, 2014:** "Sibel Edmonds and the Deep State"  
   -- by Cindy Sheehan ([_Cindy Sheehan's Soapbox_](http://cindysheehanssoapbox.com))

    *   [Audio Interview (59:45)](http://cindysheehanssoapbox.com/27/post/2014/10/sibel-edmonds-and-the-deep-state-soapbox-podcast-101914.html) ([_BFP_](http://www.boilingfrogspost.com/2014/10/23/the-lone-gladio-a-political-spy-thriller-reviews-interviews/))
    *   _Transcript (proposed)_  
.

8.  **October 27, 2014:** "_The Lone Gladio_ - Operation Gladio B: NATO-CIA-MI6 Recipe for Zbigniew Brzezinski’s Grand Chessboard" (Interview with James Corbett)  
   -- by Jim Hogue ([_House at Pooh Corner_ | WGDR](http://www.wgdr.org/house-at-pooh-corner/))

    *   [Audio Interview (63:42)](http://www.boilingfrogspost.com/2014/10/27/the-lone-gladio-operation-gladio-b-nato-cia-mi6-recipe-for-zbigniew-brzezinskis-grand-chessboard/)
    *   _Transcript (proposed)_  
.

9.  **October 28, 2014:**  "Sibel Edmonds on _The Lone Gladio_"  
   -- by Tom Secker ([_ClandesTime_ | SpyCulture.com](http://www.spyculture.com/podcast/))
    *   [Audio Interview (52:52)](http://www.spyculture.com/clandestime-episode-040-sibel-edmonds-on-the-lone-gladio/) ([_BFP_](http://www.boilingfrogspost.com/2014/10/28/clandestime-sibel-edmonds-on-the-lone-gladio/)) ([YouTube](https://www.youtube.com/watch?v=JVmgd0EkXFg))
    *   **Subtitled/Captioned Video** ([in process](http://amara.org/en/videos/09pbgJMoPO7l/info/sibel-edmonds-on-the-lone-gladio-clandestime-040/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/t5Quk7QJ))
    *   _Transcript (proposed)_  
.

10. **November 3, 2014:** "Sibel Edmonds Goes Deep on Her New Novel, and the Deep State"
   -- by Peter B. Collins ([The Peter B. Collins Show](http://www.peterbcollins.com))

    *   [Audio Interview (78:48)](http://www.peterbcollins.com/2014/11/03/10690/) ([_BFP_](http://www.boilingfrogspost.com/2014/10/23/the-lone-gladio-a-political-spy-thriller-reviews-interviews/)):
    *   _Transcript (proposed)_  
.

11.  **November 10, 2014:** "Operation Gladio B: A Roundtable Discussion with Pearse Redmond, Tom Secker & Sibel Edmonds"
   -- by Pearse Redmond ([_Porkins Policy Radio | Porkins Policy Review_](https://porkinspolicyreview.wordpress.com/category/porkins-policy-radio/))

    *   [Audio Roundtable Discussion (81:25)](http://porkinspolicyreview.wordpress.com/2014/11/10/porkins-policy-radio-ep-30-gladio-b-rountable-with-sibel-edmonds-and-tom-secker/) ([_BFP_](http://www.boilingfrogspost.com/2014/11/10/operation-gladio-b-a-roundtable-discussion-with-pearse-redmond-tom-secker-sibel-edmonds/)) ([YouTube](https://www.youtube.com/watch?v=hFIpEMiOwKY))
    *   **Subtitled/Captioned Video** ([in process](http://www.amara.org/en/videos/uyKTYMmj8Nft/info/porkins-policy-radio-ep-30-gladio-b-roundtable-with-sibel-edmonds-and-tom-secker/)) ([Raw TXT dump of uncorrected draft](http://pastebin.com/MVk6amrX))
    *   _Transcript (proposed)_  
.
* * *

## Total: 44h:49m:32s

[![Creative Commons License][image-1]][1]  
This work by "Adjuvant" is licensed under a [Creative Commons Attribution 4.0 International License][1].

[1]:    http://creativecommons.org/licenses/by/4.0
[image-1]:  http://i.creativecommons.org/l/by/4.0/88x31.png